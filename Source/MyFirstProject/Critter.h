// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Critter.generated.h"

UCLASS()
class MYFIRSTPROJECT_API ACritter : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ACritter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere)
	// class UStaticMeshComponent* MeshComponent; // Declare the pointer MeshComponent as a UStaticMeshComponent
	class USkeletalMeshComponent* MeshComponent; // Declare the pointer MeshComponent as a UStaticMeshComponent

	UPROPERTY(EditAnywhere)
	class UCameraComponent* Camera; // Declare the pointer Camera as a UCameraComponent

	UPROPERTY(EditAnywhere, Category = "Pawn Movement")
	float MaxSpeed; // Declare Max Speed

private:

	void MoveForward(float Value); // Declare MoveForward Function
	void MoveRight(float Value); // Declare MoveRight Function

	FVector CurrentVelocity; // Declare CurrentVelocity
};
