// Fill out your copyright notice in the Description page of Project Settings.


#include "MyActorTraining.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AMyActorTraining::AMyActorTraining()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CustomStaticMesh"));

	// Initialize variables with default values
	InitialLocation = FVector(0.0f);
	PlacedLocation = FVector(0.0f);
	WorldOrigin = FVector(0.0f, 0.0f, 0.0f);

	InitalDirection = FVector(0.0f, 0.0f, 0.0f);

	InitialForce = FVector(0.0f, 0.0f, 0.0f);
	InitialTorque = FVector(0.0f, 0.0f, 0.0f);

	bInitializeFloaterLocations = false;
	bShouldFloat = false;

	RunningTime = 0.0f;

	Amplitude = 1.0f;
	TimeStretch = 1.0f;
	Translation = 0.0f;
}

// Called when the game starts or when spawned
void AMyActorTraining::BeginPlay()
{
	Super::BeginPlay();

	// float initialX = FMath::FRand(); // Sets initalX to a random value
	// float initialY = FMath::FRand(); // Sets initalY to a random value
	// float initialZ = FMath::FRand(); // Sets initalZ to a random value

	float initialX = FMath::FRandRange(-500.0f, 500.0f); // Sets initalX to a random value within a range
	float initialY = FMath::FRandRange(-500.0f, 500.0f); // Sets initalY to a random value within a range
	float initialZ = FMath::FRandRange(-500.0f, 500.0f); // Sets initalZ to a random value within a range

	// Set the inital location used in the function to the random value
	InitialLocation.X = initialX; 
	InitialLocation.Y = initialY;
	InitialLocation.Z = initialZ;

	// InitialLocation *= 500.f; // Scale Inital Location by 500 to increase effects

	PlacedLocation = GetActorLocation(); // Set Placed Location to Actor Location when you hit Play

	if (bInitializeFloaterLocations)
	{
		SetActorLocation(InitialLocation); // Actor will be set to Inital Location if toggle is set
	}


	// FVector LocalOffset = FVector(200.0f, 0.0f, 0.0f); // Translate the Actor X, Y, Z

	// FVector InitialForce = FVector(20000000.0f, 0.0f, 0.0f);

	// StaticMesh->AddForce(InitialForce);
	// StaticMesh->AddTorque(InitialTorque);

	// FRotator Rotation = FRotator(0.0f, 0.0f, 0.0f); // Rotate the Actor Pitch, Yaw, Roll
	// AddActorLocalOffset(LocalOffset, true, &HitResult);
	// AddActorWorldOffset(LocalOffset, true, &HitResult);

	// AddActorLocalRotation(Rotation);
	
}

// Called every frame
void AMyActorTraining::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// If Should Float toggle is checked
	if (bShouldFloat)
	{
		FVector NewLocation = GetActorLocation(); // Set New Location to Actor Location every frame

		// NewLocation.Z = NewLocation.Z + Amplitude * FMath::Sin(TimeStretch * RunningTime) + Translation;

		// Apply sine wave to Actors Location forcing a new location along the sine wave as time moves forward
		NewLocation.X = NewLocation.X + Amplitude * FMath::Sin(TimeStretch * RunningTime) + Translation;
		NewLocation.Y = NewLocation.Y + Amplitude * FMath::Cos(TimeStretch * RunningTime) + Translation;


		SetActorLocation(NewLocation); // Update Actor location every frame with New Location

		RunningTime += DeltaTime;

		// FHitResult HitResult;
		// Turn on sweeping to detect collisions without enabling physics. Prevents passthrough
		// AddActorLocalOffset(InitalDirection, true, &HitResult); 

		// FVector HitLocation = HitResult.Location;

		// UE_LOG(LogTemp, Warning, TEXT("Hit Location: X = %f, Y = %f, Z = %f"), HitLocation.X, HitLocation.Y, HitLocation.Z);
	}

}

