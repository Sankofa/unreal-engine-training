// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyActorTraining.generated.h"

UCLASS()
class MYFIRSTPROJECT_API AMyActorTraining : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyActorTraining();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Actor Mesh Components")
	UStaticMeshComponent* StaticMesh; // Declares a pointer StaticMesh to type UStaticMeshComponent

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "Floater Variables")
	FVector InitialLocation; // Location used by SetActorLocation() When BeginPlay() is called

	UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite, Category = "Floater Variables")
	FVector PlacedLocation; // Location of the actor when dragged in from the editor

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Floater Variables")
	FVector WorldOrigin; // Location of the actors world origin

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Floater Variables")
	FVector InitalDirection; // Vector of actors inital direction

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Floater Variables")
	bool bShouldFloat; // Toggle for enabling float function

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Floater Variables")
	bool bInitializeFloaterLocations; // Toggle for enabling Inital location function

	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = "Floater Variables")
	FVector InitialForce; // Inital Force Vector X, Y, Z

	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = "Floater Variables")
	FVector InitialTorque; // Initial Torque Vector Pitch, Yaw, Roll

private:

	float RunningTime; // Variable to store time from start

	// Amplitude[sin](TimeStretch) + Translation
	UPROPERTY(EditAnywhere, Category = "Floater Variables | Wave Parameters")
	float Amplitude;
	UPROPERTY(EditAnywhere, Category = "Floater Variables | Wave Parameters")
	float TimeStretch;
	UPROPERTY(EditAnywhere, Category = "Floater Variables | Wave Parameters")
	float Translation;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
