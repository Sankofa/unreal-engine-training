// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "Explosive.generated.h"

/**
 * 
 */
UCLASS()
class MYFIRSTPROJECT_API AExplosive : public AItem
{
	GENERATED_BODY()
public:

		AExplosive();

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
		float Damage;

		/* If you are inheriting a function from a parent class then you do not use UFUNCTION(and properties) and you must override. We are inheriting from Item.h */
		virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

		virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;
		
};
